import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Activation, LSTM, Embedding, Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras import optimizers
from keras import backend as K
from keras.initializers import glorot_uniform, uniform
import os
from learning.model import Model
from copy import deepcopy
#os.environ['CUDA_VISIBLE_DEVICES'] = '0'

class DNNModel(Model):
    def __init__(self, update_rule, loss_function):
        self.update_rule = update_rule
        self.loss_function = loss_function
        self.records = []
        self.labels = []

    def init_core(self):
        pass

    @property
    def weights(self):
        if self.core is not None:
            return self.core.get_weights()
        else:
            return []

    # the way to replace weights in one network with the weights from another
    def clone(self, other):
        self.core.set_weights(other.weights)

    def distance(self, other):
        w1 = self.flatten(self.weights)
        w2 = self.flatten(other.weights)
        dist = np.linalg.norm(w1-w2)
        return dist

    def norm(self):
        w1 = self.flatten(self.weights)
        return w1.norm()

    def getPredictionScore(self, record):
        score = self.core.predict(np.asarray([record]))
        return score

    def addExample(self, record, label):
        self.records.append(record)
        self.labels.append(label)

    def getExamples(self):
        return self.records, self.labels

    def cleanBatch(self):
        self.records = []
        self.labels = []

    def batchFull(self):
        return len(self.records) == self.batch_size

    # implement addition of weights of two networks
    def add(self, other):
        w1 = self.weights
        w2 = other.weights
        new_weights = []
        for i in range(len(w1)):
            new_weights.append(w1[i] + w2[i])
        self.set_weights(new_weights)

    def flatten(self, w):
        flat_w = []
        for wi in w:
            flat_w += np.ravel(wi).tolist()
        return np.asarray(flat_w)

    def set_weights(self, w):
        if self.core is not None:
            self.core.set_weights(w)

    # multiply the weights of a model by number
    def scalarMultiply(self, scalar):
        w = self.weights
        for wi in w:
            wi *= scalar
        self.set_weights(w)

    def getInitParam(self):
        return {}

    def getModelSize(self):
        w = self.flatten(self.weights)
        return w.shape[0]

    def getModelParameters(self):
        ## what should be here?
        return "None"

    def getModelIdentifier(self):
        return "generic DNN model (please replace)"

    # randomly initialized model
    def getZeroModel(self):
        kwargs = self.getInitParam()
        zeroModel = self.__class__(**kwargs)
        zeroModel.init_core()
        return zeroModel

    def fromRecord(self, record):
        model = self.getZeroModel()
        model.clone(record)
        return model

class LstmModel(DNNModel):
    def __init__(self, unroll_steps, update_rule, loss_function, vocab_size, batch_size = 1):
        self.modelType = "deep_nn_lstm"
        self.unroll_steps = unroll_steps
        self.batch_size = batch_size
        self.vocab_size = vocab_size
        self.emb_size = 8
        self.hidden_size = 256
        self.num_layers = 2
        self.max_grad_norm = 5
        self.update_rule = update_rule
        self.loss_function = loss_function
        self.records = []
        self.labels = []

    def init_core(self):
        static_initializer = glorot_uniform(seed=42)
        network = Sequential()
        network.add(Embedding(self.vocab_size, self.emb_size, input_length=self.unroll_steps, embeddings_initializer=uniform(seed=42)))
        network.add(LSTM(self.hidden_size, return_sequences=True, input_shape=(None, self.emb_size), kernel_initializer=static_initializer))
        for i in range(self.num_layers - 2):
            network.add(LSTM(self.hidden_size, return_sequences=True, kernel_initializer=static_initializer))
        network.add(LSTM(self.hidden_size, kernel_initializer=static_initializer))
        network.add(Dense(self.vocab_size, kernel_initializer=static_initializer))
        network.add(Activation('softmax'))
        ## might be dangerous
        optimizer = eval("optimizers." + self.update_rule.name + "(clipnorm=" + str(self.max_grad_norm) + ", lr=" + str(self.update_rule.learning_rate) + ")")
        network.compile(loss=self.loss_function.name, optimizer=optimizer)
        self.core = network
        self.core.summary()

    def getInitParam(self):
        return {'unroll_steps':self.unroll_steps, 'update_rule':self.update_rule, 'loss_function':self.loss_function, 'vocab_size':self.vocab_size, 'batch_size':self.batch_size}

    def getModelIdentifier(self):
        return "lstmNetwork"

class CnnModel(DNNModel):
    def __init__(self, update_rule, loss_function, batch_size = 1):
        self.modelType = "deep_nn_cnn"
        self.batch_size = batch_size
        self.num_classes = 10
        self.img_rows = 28
        self.img_cols = 28
        self.input_shape = (self.img_rows, self.img_cols, 1)
        self.update_rule = update_rule
        self.loss_function = loss_function
        self.records = []
        self.labels = []

    def init_core(self):
        np.random.seed(42)
        tf.set_random_seed(42)
        static_initializer = glorot_uniform(seed=42)
        network = Sequential()
        network.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=self.input_shape, kernel_initializer=static_initializer))
        network.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer=static_initializer))
        network.add(MaxPooling2D(pool_size=(2, 2)))
        network.add(Dropout(0.25, seed=42))
        network.add(Flatten())
        network.add(Dense(128, activation='relu', kernel_initializer=static_initializer))
        network.add(Dropout(0.5, seed=42))
        network.add(Dense(self.num_classes, activation='softmax', kernel_initializer=static_initializer))
        ## might be dangerous
        optimizer = eval("optimizers." + self.update_rule.name + "(lr=" + str(self.update_rule.learning_rate) + ")")
        network.compile(loss=self.loss_function.name, optimizer=optimizer)
        self.core = network
        self.core.summary()

    def getInitParam(self):
        return {'update_rule':self.update_rule, 'loss_function':self.loss_function, 'batch_size':self.batch_size}

    def getModelIdentifier(self):
        return "cnnNetwork"

class MlpModel(DNNModel):

    def __init__(self, update_rule, loss_function, batch_size = 1):
        self.modelType     = "deep_nn_mlp"
        self.batch_size    = batch_size
        self.num_classes   = 10
        self.input_length  = 28*28
        self.input_shape   = (self.input_length,)
        #self.num_layers    = 4
        self.max_grad_norm = 5
        self.update_rule   = update_rule
        self.loss_function = loss_function
        self.records       = []
        self.labels        = []

    def init_core(self):
        np.random.seed(42)
        tf.set_random_seed(42)
        static_initializer = glorot_uniform(seed=42)
        
        network = Sequential()
        network.add(Dense(512,              activation='relu',    kernel_initializer=static_initializer, input_shape=self.input_shape))
        network.add(Dropout(0.2,seed=42))
        network.add(Dense(512,              activation='relu',    kernel_initializer=static_initializer))
        network.add(Dropout(0.2,seed=42))
        network.add(Dense(self.num_classes, activation='softmax', kernel_initializer=static_initializer))
        
        ## might be dangerous
        optimizer = eval("optimizers." + self.update_rule.name + "(lr=" + str(self.update_rule.learning_rate) + ")")
        network.compile(loss=self.loss_function.name, optimizer=optimizer)
        self.core = network
        self.core.summary()

    def getInitParam(self):
        return {'update_rule':self.update_rule, 'loss_function':self.loss_function, 'batch_size':self.batch_size}

    def getModelIdentifier(self):
        return "mlpNetwork"
    
class FullyConnectedModel(DNNModel):
    def __init__(self, update_rule, loss_function, batch_size = 1, dim=1):
        self.modelType     = "deep_nn_mlp_full"
        self.batch_size    = batch_size
        self.input_length  = dim
        self.input_shape   = (self.input_length,)
        self.update_rule   = update_rule
        self.loss_function = loss_function
        self.records       = []
        self.labels        = []

    def init_core(self):
        np.random.seed(42)
        tf.set_random_seed(42)
        static_initializer = glorot_uniform(seed=42)
        
        network = Sequential()
        network.add(Dense(256, activation='relu', kernel_initializer=static_initializer, input_shape=self.input_shape))
        network.add(Dropout(0.2,seed=42))
        network.add(Dense(64, activation='relu',    kernel_initializer=static_initializer))
        #network.add(Dropout(0.2,seed=42))
        #network.add(Dense(16, activation='relu', kernel_initializer=static_initializer))
        network.add(Dense(1, activation='sigmoid', kernel_initializer=static_initializer))
        
        ## might be dangerous
        optimizer = eval("optimizers." + self.update_rule.name + "(lr=" + str(self.update_rule.learning_rate) + ")")
        network.compile(loss=self.loss_function.name, optimizer=optimizer)
        self.core = network
        self.core.summary()

    def getInitParam(self):
        return {'update_rule':self.update_rule, 'loss_function':self.loss_function, 'batch_size':self.batch_size, "dim":self.input_length}

    def getModelIdentifier(self):
        return "mlpNetworkFull"    
    
    def getPredictionScore(self, record):
        score = self.core.predict(np.asarray([record.toNumpyArray()]))
        return score

    def addExample(self, record, label):
        self.records.append(record.toNumpyArray())
        if label == -1:
            label = 0
        self.labels.append(label)
    
def arithmetic_mean(models):
    np.seterr(all='raise')
    if len(models) == 0:
        return None
    newWeights = models[0].weights
    try:
      for model in models[1:]:
        w2 = model.weights
        for i in range(len(w2)):
            newWeights[i] += w2[i]
      scalar = (1./float(len(models)))
      for i in range(len(newWeights)):
            newWeights[i] *= scalar
    except Exception as e:
      print("Error happened! Message is ", e)
    models[0].set_weights(newWeights)
    return models[0]

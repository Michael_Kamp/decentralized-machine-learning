import numpy as np

class NeuralNetworkLossFunction():
    def __init__(self, name):
        self.name=name
        self.shortname = name
        self.eps = 1e-15

    def __call__(self, true_label, prediction):
      np.seterr(all='raise')
      try:
          if self.name == "categorical_crossentropy":
            prediction = np.squeeze(prediction)
            prediction = np.clip(prediction, self.eps, 1 - self.eps)
            #prediction = self.replaceZeroes(prediction)
            loss_value = np.sum(-true_label * np.log(prediction))
          if self.name == "binary_crossentropy":
            prediction = np.squeeze(prediction)
            prediction = np.clip([prediction], self.eps, 1 - self.eps)
            #prediction = self.replaceZeroes(np.array([prediction]))
            if hasattr(true_label, "__len__"):
                true_label = true_label[0]
            loss_value = -true_label * np.log(prediction[0]) - (1 - true_label) * np.log(1 - prediction[0])
          if self.name == "mse":
            prediction = np.squeeze(prediction)
            loss_value = np.mean((prediction - true_label)**2)
      except Exception as e:
          print("Error in calculation of loss function! Prediction is ", prediction, "error is ", e)
      return loss_value

    def replaceZeroes(self, ar):
      if len(ar) == len(ar[ar > 10**-30]):
        return ar
      min_nonzero = 10**-30
      ar[ar < 10**-30] = min_nonzero
      return ar

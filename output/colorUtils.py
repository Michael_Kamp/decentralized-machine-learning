'''
Created on 20.08.2015

@author: mkamp
'''
#import itertools
#from decimal import Decimal 
#import sys
import random


class ColorUtil:
    def __init__(self):
        pass
    
    def generateDistinctColors(self, num):
        #if num < 1:
        #    print("Error: Stop screwing around.")
        #    sys.exit()
          
        #def MidSort(lst):
        #    if len(lst) <= 1:
        #        return lst
        #    i = int(len(lst)/2)
        #    ret = [lst.pop(i)]
        #    left = MidSort(lst[0:i])
        #    right = MidSort(lst[i:])
        #    interleaved = [item for items in itertools.izip_longest(left, right) for item in items if item != None]
        #    ret.extend(interleaved)
        #    return ret
        
        # Build list of points on a line (0 to 255) to use as color 'ticks'
        #max = 255 #max is < 255 to avoid too light colors.
        #segs = int(num**(Decimal("1.0")/2))
        #step = int(max/segs)
        #p = [(i*step) for i in range(1,segs)]
        #points = [0,max]
        #points.extend(MidSort(p))
        
        # Not efficient!!! Iterate over higher valued 'ticks' first (the points
        #   at the front of the list) to vary all colors and not focus on one channel.
        #colors = ["#%02X%02X%02X" % (points[0], points[0], points[0])]
        #myrange = 0
        #total = 1
        #while total < num and range < len(points):
        #    myrange += 1
        #    for c0 in range(myrange):
        #        for c1 in range(myrange):
        #            for c2 in range(myrange):
        #                if total >= num:
        #                    break
        #                if points[c0] + points[c1] + points[c2] > 630: #in order to avoid too light colors
        #                    continue            
        #                c = "#%02X%02X%02X" % (points[c0], points[c1], points[c2])
        #                if c not in colors:
        #                    colors.append(c)
        #                    total += 1
        

        def get_random_color(pastel_factor = 0.5):
            return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

        def color_distance(c1,c2):
            return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

        def generate_new_color(existing_colors,pastel_factor = 0.5):
            max_distance = None
            best_color = None
            for i in range(0,100):
                color = get_random_color(pastel_factor = pastel_factor)
                if not existing_colors:
                    return color
                best_distance = min([color_distance(color,c) for c in existing_colors])
                if not max_distance or best_distance > max_distance:
                    max_distance = best_distance
                    best_color = color
            return best_color


        colors = []
        for i in range(0,num):
            colors.append(generate_new_color(colors, pastel_factor = 0.3))
        return colors


if __name__ == "__main__":
    colors = ColorUtil().generateDistinctColors(5)
    print(colors)

'''
Created on 20.01.2013

@author: Mario Boley
'''

import matplotlib.pyplot as plt
from output.parseOutput import read_results_column, RESULT_FILENAME, ENV_ID_COLUMN, ENV_ERROR_COLUMN, ENV_LABEL_COLUMN, ENV_TYPE_COLUMN, ENV_PARAM_COLUMN, ENV_COMM_MESSAGE_COLUMN
from output.plotting.plotting import CHART_DIMENSIONS, FILE_FORMAT, save_chart, generate_color
import matplotlib
from statsmodels.base.model import Results

def generate(result_root_folder,predictive_performance_column=ENV_ERROR_COLUMN, plotBaseLines = True, loss_function = None):
    print("Plotting Performance")
    result_filename=result_root_folder+RESULT_FILENAME
    envs = read_results_column(result_filename,ENV_ID_COLUMN)
    labels = read_results_column(result_filename, ENV_LABEL_COLUMN)
    types = read_results_column(result_filename,ENV_TYPE_COLUMN)
    _ = read_results_column(result_filename,ENV_PARAM_COLUMN)
    values = read_metrics(envs,result_filename,predictive_performance_column)

    x_label = "Cumulative Communication (bytes)"
    y_label = "Cumulative Error"

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plot_points(values, labels, types, markers_size)
    if plotBaseLines:
        plot_baselines(types,result_filename,predictive_performance_column)

    plt.title(title)
    save_chart(result_root_folder+'charts/performance.%s' % (FILE_FORMAT))

def read_metrics(envs,result_filename,predictive_performance_column):
    result = []
    errors = read_results_column(result_filename, predictive_performance_column)
    communication = read_results_column(result_filename, ENV_COMM_MESSAGE_COLUMN)
    for i in range(len(envs)):
        result.append((float(communication[i]), float(errors[i])))
    print(envs)
    print(result)
    print(errors)
    return result

def read_cumulative_error(file_name,loss_function):
    rounds = []
    values = []
    currentError=0.0
    currentRound=0
    handle = open(file_name, 'r')
    for line in handle:
        parts = line.strip().split("\t")
        currentError+=loss_function(float(parts[1]),float(parts[2]))
    return currentError

def plot_baselines(types, result_file,predictive_performance_column):
    baseline_markers = ['--', '-.', '-',':','--', '-.', '-',':']
    baseline_color = 'k'

    marker_index = 0
    for i in range(len(types)):
        env_type = types[i]
        if env_type == 'baseline':
            marker = baseline_markers[marker_index]
            marker_index += 1
            y = read_results_column(result_file,predictive_performance_column)[i]
            name  = read_results_column(result_file,1)[i]
            plt.axhline(float(y), color = baseline_color, linestyle = marker, label=r''+name)

def plot_baselines_annotation(envs, types, labels, result_file,predictive_performance_column):
    _,xmax,_,_ = plt.axis()
    x_annotation_position = 0.9 * xmax
    for i in range(len(envs)):
        env_type = types[i]
        if env_type == 'baseline':
            y = read_results_column(result_file,predictive_performance_column)[i]
            annotation = read_results_column(result_file,ENV_LABEL_COLUMN)[i]
            plt.annotate(annotation, xy = (x_annotation_position, y), xytext = (0, 15),
                        textcoords = 'offset points', ha = 'right', va = 'top')

def plot_points(values, labels, types, markers_size):
    for i in range(len(values)):
        color = generate_color(i, len(values))
        if types[i] != 'baseline':
            if types[i] == 'static':
                marker = 's'
            else:
                marker = '*'
            x,y = values[i]
            plt.scatter(x*4, y, s = markers_size, color = color, marker = marker, label = r''+labels[i], alpha = 0.7)


MARKERS = ['x','o','s','^','v','*','p','+']
COLORS = ['0.4','0.3','0.6']
COLOR = ['RED', 'BLUE', 'GREEN','YELLOW']
title = ''
markers_size = 400

if __name__ == "__main__":
    generate("./testdata/")

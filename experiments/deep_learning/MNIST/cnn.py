from inputs.cnnMnist import CnnMnist
from inputs.stoppingConditions import MaxNumberOfExamplesCondition
from learning.neuralNetworkLossFunction import NeuralNetworkLossFunction
from learning.neuralNetworkUpdateRule import NeuralNetworkUpdateRule
from learning.DNNModel import CnnModel, arithmetic_mean
from environments import PredictionEnvironment
from synch.synchronization import NoSyncOperator, CentralSyncOperator, StaticSyncOperator, HedgedDistBaseSync
import experiment

def main():
    numberOfNodes   = 100    
    anchorBatchSize = 10
    
    inputStream     = CnnMnist(data_file = 'mnist_train.csv', nodes = numberOfNodes)
    learningRate    = 0.1
    lossFunction    = NeuralNetworkLossFunction('categorical_crossentropy')
    updateRule      = NeuralNetworkUpdateRule('SGD', lossFunction, learningRate)
    updateRule1      = NeuralNetworkUpdateRule('SGD', lossFunction, 0.25)
    
    model     = CnnModel(update_rule = updateRule, loss_function = lossFunction, batch_size = anchorBatchSize)
    model1     = CnnModel(update_rule = updateRule1, loss_function = lossFunction, batch_size = anchorBatchSize)
    
    envs    =   [
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         syncOperator    = NoSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule,
                                         model           = model,
                                         syncOperator    = CentralSyncOperator(),
                                         serial          = True,
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(0.3),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = 2*anchorBatchSize,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(0.7),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = 4*anchorBatchSize,
                                         syncOperator    = StaticSyncOperator(),
                                         modelAggregation= arithmetic_mean),
                 PredictionEnvironment(  numberOfNodes   = numberOfNodes,
                                         updateRule      = updateRule1,
                                         model           = model1,
                                         batchSizeInMacroRounds  = anchorBatchSize,
                                         syncOperator            = HedgedDistBaseSync(1),
                                         modelAggregation= arithmetic_mean),
                 ]

    experiment.run(inputStream, envs, MaxNumberOfExamplesCondition(numberOfNodes*14000))

if __name__ == "__main__":
    main()

